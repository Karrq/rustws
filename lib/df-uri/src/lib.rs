#[cfg(test)]
mod tests;

use std::collections::HashMap;

#[derive(Debug)]
pub enum URIType{
    File(String),
    CGI(CGICall)
}

#[derive(Debug)]
pub struct CGICall{
    name : String,
    args: HashMap<String, Option<String>>
}

impl URIType {
    pub fn parse(uri : &str) -> Self {
        let parsed_uri : URIType;
        
        match uri.find('&'){
            None => {
                parsed_uri = URIType::File(String::from(uri));
            },
            Some(n) => {
                parsed_uri = URIType::CGI(CGICall::with_index(&uri, n));
            },
        }

        parsed_uri
    }    
}

impl CGICall{

    fn get_args(s : &str) -> HashMap<String, Option<String>> {
        //let's be verbose about types
        let mut args : HashMap<String, Option<String>> = HashMap::new();
        
        //get each &name... argument
        for s in s.split('&') {
            if s == "" {
                //the iterator will also return an empty string
                //as the last element
                continue;
            }
            //separate name and value
            let mut parts = s.split('=');
            //with no '=' in the string
            //the first value returned is the whole string
            let name = String::from(parts.next().unwrap());
            let value;
            match parts.next() {
                None => {
                    value = None;
                }
                
                Some(s) => {
                    value = Some(String::from(s));
                }
            }
        
            args.insert(name, value);
        }
        
        args
    }
    
    pub fn with_index(uri: &str, n: usize) -> Self {
        /*
            name = "/..."
            string_args = "&arg1&arg2=val1&arg3=val2..."
        */
        let (name, string_args) : (&str, &str) = uri.split_at(n);
        
        CGICall{
            name : String::from(&name[1..]),
            args : CGICall::get_args(string_args),
        }
    }

    pub fn name(&self) -> &str {
        &self.name
    }

    pub fn args(&self) -> &HashMap<String, Option<String>>{
        &self.args
    }
    
}