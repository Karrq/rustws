extern crate df_uri;

#[cfg(test)]
mod tests;

pub mod request;
pub mod response;

#[derive(Debug)]
pub enum HttpMethod {
    GET,
    POST
}

#[derive(Debug, Clone, Copy)]
pub enum HttpVersion {
    Unknown,
    V1_1,
    V2_0,
}


impl HttpMethod{
    fn from_string(s : Option<&str>) -> Self {
        match s {
            Some("GET") => HttpMethod::GET,
            Some("POST") => HttpMethod::POST,
            _ => unreachable!(),
        }
    }
}

impl ToString for HttpMethod {
    fn to_string(&self) -> String{
        unimplemented!();
    }
}


impl HttpVersion{
    fn from_string(s : Option<&str>) -> Self {
        match s {
            Some("HTTP/1.1") => HttpVersion::V1_1,
            Some("HTTP/2.0") => HttpVersion::V2_0,
            Some(_) => HttpVersion::Unknown,
            _ => unreachable!(),
        }
    }

    fn as_bytes(self) -> Vec<u8> {
        let mut bytes = Vec::new();

        bytes.extend(self.to_string().as_bytes().iter());

        bytes
    }
  
}

impl ToString for HttpVersion {
    fn to_string(&self) -> String{
        match self {
            HttpVersion::Unknown => {
                panic!("Trying to send an unknown http version!");
            },

            HttpVersion::V1_1 => "HTTP/1.1".to_string(),
            HttpVersion::V2_0 => "HTTP/2.0".to_string(),            
        }
    } 
}