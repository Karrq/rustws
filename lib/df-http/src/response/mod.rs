#[cfg(test)]
mod tests;

use std::net::TcpStream;
use std::collections::HashMap;

use crate::*;

#[derive(Debug)]
pub enum HttpStatus {
    Ok,
    Forbidden,
    AccessDenied,
    NotFound,
    InternalServerError,
}

#[derive(Debug)]
pub struct HttpResponse{
    version : HttpVersion,
    status : HttpStatus,
    phrase : Option<String>,
    headers : HashMap<String, String>,
    message : Vec<u8>
}

impl HttpStatus {
    fn as_bytes(&self) -> Vec<u8> {
        let mut bytes = Vec::new();
        bytes.extend(self.to_string().as_bytes());
        bytes
    }
}

use std::io::Write;
impl HttpResponse {
    
    fn as_bytes(&self) -> Vec<u8> {
        let mut bytes : Vec<u8> = Vec::new();
        let newline = b"\r\n";

        bytes.extend(&self.version.as_bytes()); // get http version
        bytes.extend(b" ");
        bytes.extend(&self.status.as_bytes()); //get http status
        bytes.extend(b" ");

        if let Some(s) = &self.phrase {
            bytes.extend(s.as_bytes());
        }

        bytes.extend(newline); //add newline before headers

        for (key, val) in &self.headers {
            let s = String::new() + key + ": " + val;
            bytes.extend(s.as_bytes());
            bytes.extend(newline); //add newline after each header
        }

        bytes.extend(newline); //add newline before message body

        bytes.extend(&self.message);

        bytes
    }
    
    pub fn set_version(self, v : HttpVersion) -> Self {
        HttpResponse {
            version : v,
            ..self
        }
    }

    pub fn set_status(self, s : HttpStatus) -> Self{
        HttpResponse{
            status : s,
            ..self
        }
    }

    pub fn set_phrase(self, p : &str) -> Self{
        HttpResponse{
            phrase : Some(p.to_owned()),
            ..self
        }
    }

    pub fn set_headers(self, h : HashMap<String, String>) -> Self{
        HttpResponse{
            headers : h,
            ..self
        }
    }

    pub fn set_message(self, msg : &[u8]) -> Self{
        let mut v = Vec::new();
        v.extend(msg.iter());

        HttpResponse{
            message : v,
            ..self
        }
    }

    pub fn get_message(&self) -> &[u8] {
        &self.message
    }

    pub fn append_message(&mut self, extra : &[u8]) {
        self.message.extend(extra);
    }

    pub fn write(&self, conn : &mut TcpStream) -> Result<(), String> {
        let mut result = match conn.write(&self.as_bytes()) {
            Ok(n) if n > 0  => Ok(()),
            Ok(n) if n == 0 => Err("Error when transmitting data".to_owned()),
            Err(e) => Err(format!("IO Error: {}", e)),
            Ok(_) => unreachable!(),
        };

        if result == Ok(()) {
            match conn.flush() {
                Ok(_) => {
                    result = Ok(());
                },

                Err(e) => {
                    result = Err(format!("IO Error: {}", e));
                },
            }
        }

        result
    }
}

impl ToString for HttpStatus {
    fn to_string(&self) -> String{
        match self {
            HttpStatus::Ok => "200".to_string(),
            HttpStatus::Forbidden => "403".to_string(),
            HttpStatus::AccessDenied => "401".to_string(),
            HttpStatus::NotFound => "404".to_string(),
            HttpStatus::InternalServerError => "505".to_string(),
        }
    }
}

impl ToString for HttpResponse {
    fn to_string(&self) -> String {
        let mut result = String::new();

        //first line
        //version status-code phrase \r\n
        result += &self.version.to_string();
        result += " ";
        result += &self.status.to_string();
        result += " ";
        
        if let Some(phrase) = &self.phrase {
            result += phrase;
        }

        result += "\r\n";

        //headers \r\n

        for (key, val) in &self.headers {
            let s = String::new() + key + ": " + val;
            result += &s;
            result += "\r\n"; //new line each header
        }

        result += "\r\n";

        //next up is the body

        for &b in &self.message {
            let b : [u8; 1] = [b];
            result += match std::str::from_utf8(&b[..]) {
                Ok(v) => v,
                Err(_) => "", //unable to parse that byte as utf8, discarding.
            };
        }

        //string is fully formed now
        result
    }
}

impl Default for HttpResponse {
    fn default() -> Self {
        HttpResponse{
            version : HttpVersion::Unknown,
            status : HttpStatus::Ok,
            phrase : None,
            headers : HashMap::new(),
            message : Vec::new(),
        }
    }
}