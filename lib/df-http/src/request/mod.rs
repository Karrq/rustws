#[cfg(test)]
mod tests;

use std::collections::HashMap;

use df_uri::URIType;
use crate::*;

#[derive(Debug)]
pub struct HttpRequest{
    method : HttpMethod,
    uri : String,
    version : HttpVersion,
    headers : HashMap<String, String>,
    message : Option<Vec<u8>>
}


impl HttpRequest{

    pub fn from_string(req: &str) -> Self {
        let mut lines = req.lines();
        let line = lines.next().unwrap();
        
        /* parse first line */
        let mut words = line.split_whitespace();

        //first token is the http method
        let method = HttpMethod::from_string(words.next());

        //next token should be the URI
        let uri = String::from(words.next().unwrap());

        //next token is the http version
        let version = HttpVersion::from_string(words.next());

        /* parsing headers */
        let mut headers : HashMap<String, String> = HashMap::new();
        for line in &mut lines {
            match line {
                "" => {
                    //we reached the end of parsing the headers
                    //since before the request body there's another new line
                    break;
                },
                _ => {
                    let index = line.find(':').unwrap();
                    let (key, val) = line.split_at(index);

                    //skipping the first 2 characters
                    //1 is ':' and the other ' '
                    headers.insert(key.to_string(), val[2..].to_string());
                },      
            }
        }

        /* 
            only for POST 
            should be the rest of the response
        */
        let message : Option<Vec<u8>>;
        match method {
            HttpMethod::GET => {
                message = None;
            },

            HttpMethod::POST => {
                let mut msg = String::new();
                for line in lines {
                    //we might still want the \r\n from the body
                    //so we add it again
                    msg = msg + "\r\n" + line;
                }

                let mut msg_v : Vec<u8> = Vec::new();
                msg_v.extend(msg.bytes());
                message = Some(msg_v);
            },

        }

        HttpRequest{
            method : method,
            uri : uri,
            version : version,
            headers : headers,
            message : message,
        }
    }

    pub fn uri(&self) -> URIType {
        URIType::parse(&self.uri)
    }

    pub fn version(&self) -> HttpVersion {
        self.version
    }

}