use std::thread;
use std::sync::{Arc, Mutex, mpsc};

pub trait FnBox {
    fn call_box(self: Box<Self>);
}

impl<F: FnOnce()> FnBox for F {
    fn call_box(self: Box<F>){
        (*self)()
    }
}

pub struct Worker{
    id : usize, //help distinguish the different workers
    pub thread : Option<thread::JoinHandle<()>>,
}

pub type Job = Box<dyn FnBox + Send + 'static>;

pub enum Message {
    NewJob(Job),
    Terminate
}


impl Worker {
    pub fn new(id: usize, receiver: Arc<Mutex<mpsc::Receiver<Message>>>) -> Worker {
        Worker {
            id,
            thread : Some(thread::spawn(move ||{
                loop {
                    //TODO add more robus error handling
                    let job = receiver.lock().unwrap().recv().unwrap();
                    match job {
                        Message::NewJob(job) => job.call_box(),
                        Message::Terminate => { break; },
                    }
                }
            }))
        }    
    }
}