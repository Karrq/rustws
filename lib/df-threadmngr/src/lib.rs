#[cfg(test)]
mod tests;

mod worker;
use self::worker::*;

use std::sync::{Arc, Mutex, mpsc};

//TODO Add more documentation
//also use in another project
pub struct ThreadPool{
    workers : Vec<worker::Worker>, // the connection handling closures don't return anything
    tx : mpsc::Sender<worker::Message>, //sending closures
}

#[derive(Debug)]
pub enum PoolCreationError{
    NoThreads
}

impl ThreadPool {
    /// Create a new ThreadPool
    /// 
    /// The size is the number of threads in the pool
    /// 
    /// # Errors
    /// PoolCreationError::NoThreads if size is 0
    pub fn new(size: usize) -> Result<ThreadPool, PoolCreationError> {
       if size > 0{
           let mut workers = Vec::with_capacity(size); //creating a vec to store the threads in
           let (tx, rx) = mpsc::channel();

           let rx = Arc::new(Mutex::new(rx));

           for id in 0..size {
               //create threads and store them
               workers.push(Worker::new(id, Arc::clone(&rx)));
           }

           Ok(ThreadPool{ workers, tx })
       }else{
           Err(PoolCreationError::NoThreads)
       }
    }
    
    //TODO Add more documentation
    pub fn exec<F>(&self, func: F)
        where
            F: FnOnce() + Send + 'static
    {
        let j = Box::new(func);

        self.tx.send(Message::NewJob(j)).unwrap();
        //send only fails if there's no receiving end
        //no receiving end means no threads running
        //and that can't be because they only stop running when the ThreadPool is dropped
        //this unwrap will never panic, but the compiler can't know that
    }
}

use std::fmt;
impl fmt::Display for PoolCreationError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let result = match self {
            PoolCreationError::NoThreads => "Can't create a pool with zero threads!"
        };

        write!(f, "{}", result)
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        for _ in &mut self.workers {
            //TODO add more robus error handling
            self.tx.send(Message::Terminate).unwrap();
        }

        for worker in &mut self.workers {         
            if let Some(thread) = worker.thread.take() {
                //TODO add more robus error handling
                thread.join().unwrap();
            }
        }
    }
}