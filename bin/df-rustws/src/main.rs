use std::io::prelude::*;

use std::net::{TcpListener, TcpStream};

use std::process;

use std::fs;

use std::time::Duration;
use std::thread;

extern crate df_http;
extern crate df_uri;
extern crate df_threadmngr;

use df_http::request::*;
use df_http::response::*;

//TODO try another thread pool crate on crates.io
use df_threadmngr::ThreadPool;

use df_uri::*;

fn main() {
    let listener; 
    let ip = {
        let mut args = std::env::args();
        args.next(); //skip arg[0]

        match args.next() {
            Some(n) => n,
            None => "127.0.0.1:8080".to_owned()
        
        }
    };

    let pool = match ThreadPool::new(5) {
        Ok(my_pool) => my_pool,
        Err(e) => {
            println!("Error when creating thread pool: {}", e);
            process::exit(1);
        },
    };

    match TcpListener::bind(ip) {
        Err(e) => {
            println!("{}", e);
            process::exit(1);
        },
        Ok(tcp) => {
            listener = tcp
        },
    }

    //get any incoming connection
    for stream in listener.incoming(){        
        match stream {
            Ok(stream) => {
                println!("Connection established");
                pool.exec(|| {
                    handle_conn(stream);
                });
            }, 
            Err(e) => {
                println!("Connection failed with error: {}", e);
            },
        }
    }
}

//handle connection
fn handle_conn(mut conn : TcpStream) {
    let mut buffer = [0; 512];

    match conn.read(&mut buffer){
        Ok(n) if n > 0 => {
            //sucessfully read from buffer
        },
        Ok(n) => {
            //n <= 0
            println!("Failed to read from connection: return value = {}", n);
            return;
        },
        Err(e) => {
            println!("Error when reading from connection: {}", e);
            process::exit(1);
        }
    }

    let req : HttpRequest = HttpRequest::from_string(String::from_utf8_lossy(&buffer[..]).as_ref());

    match req.uri() {
        URIType::File(s) => {
            serve_file(&req, conn, &s)
        },

        URIType::CGI(info) => {
            serve_cgi(&req, conn, &info)
        },
    }

}

fn serve_file(req : &HttpRequest, mut conn : TcpStream, filename : &str) {
    let mut response = HttpResponse::default().set_version(req.version()).set_status(HttpStatus::Ok).set_phrase("OK");

    let filename = match filename {
        "/" | "/index.html" => String::new() + "www/index.html",
        s => String::new() + "www" + s, 
    };

    match fs::read(filename){
        Ok(contents) => {
            response = response.set_message(&contents);
        }

        Err(ref e) if e.kind() == std::io::ErrorKind::NotFound => {
            response = response.set_status(HttpStatus::NotFound).set_phrase("Not Found");
            match fs::read("www/404.html") {
                Ok(c) => {
                    response = response.set_message(&c);
                },
                Err(_) => {
                    response = response.set_message(b"404\r\nFile not found");
                }
            }
        },

        Err(_) => {
            //eventually replace with 500.html's body
            response = response.set_status(HttpStatus::InternalServerError).set_phrase("Internal Server Error").set_message(b"500\r\nInternal Server Error");
        }
    }

    match response.write(&mut conn){
        Ok(()) => {

        },

        Err(e) => {
            println!("{}", e);
        }
    }
}

fn serve_cgi(req : &HttpRequest, mut conn : TcpStream, call : &CGICall) {
    //unused currently
    //let mut response = HttpResponse::new().set_version(req.version()).set_status(HttpStatus::Ok).set_phrase("OK");

    match call.name() {
        "slp.rust" => {
            let time;
            match call.args().get(&"time".to_owned()) {
                Some(Some(s)) => {
                    //entry found with value
                    match s.parse::<u64>(){
                        Ok(v) => {
                            //value can be parsed
                            time = Duration::from_secs(v);
                        },

                        Err(_) => {
                            //unable to parse, default
                            time = Duration::from_secs(10);
                        }
                    }
                },

                None | Some(None) => {
                    //no entry found
                    //or entry found without value
                    time = Duration::from_secs(10);
                }
            }

            thread::sleep(time);
            let response = HttpResponse::default().set_version(req.version()).set_status(HttpStatus::Ok).set_phrase("OK")
                .set_message(format!("I slept {}s", time.as_secs()).as_bytes());
            //println!("I slept {}s", time.as_secs());    

            match response.write(&mut conn){
                Ok(()) => {
                    return;
                },

                Err(e) => {
                    println!("{}", e);
                }
            }
        },
    
        "exe.rust" => {
        },

        _ => {
            //unknown CGI call
            //implement appropriate http response
        },
    }

    //default to landing page
    serve_file(req, conn, "/");
}